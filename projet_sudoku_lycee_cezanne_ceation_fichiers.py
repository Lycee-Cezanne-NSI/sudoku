﻿from projet_sudoku_lycee_cezanne_generation_solutions import *



def conversion_liste_en_texte(tableau,fichier) :
    f=open(fichier,'a')
    f.write("[")
    for i in range(TAILLE) :
        f.write("['")
        for j in range(TAILLE) :
            if tableau[TAILLE*i+j]!=0:
                f.write(str(tableau[TAILLE*i+j]))
            if j != TAILLE-1 :
                f.write("','")
            else :
                f.write("']")
        if i != TAILLE-1 :
            f.write(",\n")
    f.write("]")
    f.close()

def ecriture_fichier(texte,fichier,liste_enonces,liste_solutions) :
    f = open(fichier,'w')
    n = len(liste_enonces)
    f.write("enonce=[")
    f.close()
    for i in range(n):
        conversion_liste_en_texte(liste_enonces[i],fichier)
        if i != n-1:
            f=open(fichier,'a')
            f.write(",")
            f.close()
        f=open(fichier,'a')
        f.write("\n")
        f.close()
    f=open(fichier,'a')
    f.write("]\n")
    f.write("solution=[")
    f.close()
    for i in range(n) :
        conversion_liste_en_texte(liste_solutions[i], fichier)
        if i != n-1:
            f=open(fichier,'a')
            f.write(",")
            f.close()
        f=open(fichier,'a')
        f.write("\n")
        f.close()
    f=open(fichier,'a')
    f.write("]\n")
    f.close()
ecriture_fichier('faciles', 'facile.js',enonces_faciles,solutions_faciles)
ecriture_fichier('moyens', 'moyen.js',enonces_moyens,solutions_moyens)
ecriture_fichier('difficiles','difficile.js',enonces_difficiles,solutions_difficiles)

